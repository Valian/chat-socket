const express = require ('express')
const app = express()
const path = require('path')

//settings
app.set('port', process.env.PORT || 3000)

//static files
app.use(express.static(path.join(__dirname , 'public')))

const server = app.listen(app.get('port'), ()=>{
    console.log('Server On Port ', app.get('port'));
})

const SocketIO = require('socket.io')
const io = SocketIO(server)

//websockets
io.on('connection', (socket)=>{
    console.log('New Connection '+ socket.id)

    socket.on('message', (data)=>{
        io.sockets.emit('message', data)
    })
    socket.on('typing', (data)=>{
        socket.broadcast.emit('typing', data)
    })
})



