const socket = io()

let message     = document.getElementById('message');
let username    = document.getElementById('username');
let send        = document.getElementById('send');
let output      = document.getElementById('output');
let actions     = document.getElementById('actions');

send.addEventListener('click', function(){
    data = {
        username    : username.value,
        message     : message.value
    }
    socket.emit('message', data)
    console.log(data);
})

socket.on('message', function(data){
actions.innerHTML = ''
message.value = ''
output.innerHTML += `<p><strong>${data.username}: ${data.message}</strong></p>`
})

message.addEventListener('keypress', function(){
    socket.emit('typing', username.value)
})

socket.on('typing', function(data){
    actions.innerHTML  = `<p><em>${data} esta escribirendo...</em></p>`
})